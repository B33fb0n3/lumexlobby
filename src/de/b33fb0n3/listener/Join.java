package de.b33fb0n3.listener;

import de.b33fb0n3.utils.ItemBuilder;
import de.b33fb0n3.utils.Scoreboard;
import org.bukkit.Bukkit;
import org.bukkit.GameMode;
import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerJoinEvent;
import org.bukkit.inventory.ItemStack;

/**
 * Plugin made by B33fb0n3YT
 * 09.08.2020
 * F*CKING SKIDDER!
 * Licensed by B33fb0n3YT
 * © All rights reserved
 */


public class Join implements Listener {

    @EventHandler
    public void onJoin(PlayerJoinEvent e) {
        Player p = e.getPlayer();
        p.getInventory().clear();
        p.getInventory().setItem(0, new ItemBuilder(new ItemStack(Material.COMPASS)).setName("§cNavigator §7(Rechtsklick)").toItemStack());
        p.getInventory().setItem(1, new ItemBuilder(new ItemStack(Material.BLAZE_ROD)).setName("§a§lPlayerhider §7(Rechtsklick)").toItemStack());
        p.getInventory().setItem(4, new ItemBuilder(new ItemStack(Material.ENDER_PEARL)).setName("§b§lEnderperle §7(Rechtsklick)").toItemStack());
        p.getInventory().setItem(8, new ItemBuilder(new ItemStack(Material.SKULL_ITEM, 1, (short) 3)).setName("§e§l Freunde §7(Rechtsklick)").setSkullOwner(e.getPlayer().getName()).toItemStack());
        p.setGameMode(GameMode.ADVENTURE);
        for(Player current : Bukkit.getOnlinePlayers()) {
            if(e.getPlayer().hasMetadata("allhidden")) {
                e.getPlayer().hidePlayer(current);
            }
            Scoreboard.setScoreboard(current);
        }
    }
}
