package de.b33fb0n3.listener;

import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerMoveEvent;
import org.bukkit.util.Vector;

/**
 * Plugin made by B33fb0n3YT
 * 09.08.2020
 * F*CKING SKIDDER!
 * Licensed by B33fb0n3YT
 * © All rights reserved
 */

public class Move implements Listener {

    @EventHandler
    public void onMove(PlayerMoveEvent e) {
        if(e.getPlayer().getLocation().add(0,-1,0).getBlock().getType().equals(Material.WATER)) {
            Player p = e.getPlayer();
            double hoehe = 0.3D;
            double loenge = 3.5D;
            Vector v = p.getLocation().getDirection().normalize();
            v = v.setY(Math.max(hoehe, v.getY())).multiply(loenge);
            p.setVelocity(v);
        }
    }

}
