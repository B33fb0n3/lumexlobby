package de.b33fb0n3.listener;

import de.b33fb0n3.lumexlobby.Main;
import org.bukkit.Bukkit;
import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.inventory.InventoryClickEvent;

/**
 * Plugin made by B33fb0n3YT
 * 09.08.2020
 * F*CKING SKIDDER!
 * Licensed by B33fb0n3YT
 * © All rights reserved
 */

public class InvClick implements Listener {

    @EventHandler
    public void onClick(InventoryClickEvent e) {
        Player p = (Player) e.getWhoClicked();
        try {
            if (e.getClickedInventory() == p.getInventory() && !e.getCurrentItem().getType().equals(Material.FEATHER) && !e.getCurrentItem().getType().equals(Material.NAME_TAG)) {
                e.setCancelled(true);
                return;
            }
        } catch (Exception e1) {
            p.closeInventory();
        }
        e.setCancelled(true);
        try {
            if (e.getInventory().getTitle().equalsIgnoreCase("§cNavigator")) {
                p.chat("/warp " + e.getRawSlot());
            }
            switch (e.getCurrentItem().getItemMeta().getDisplayName()) {
                case "§aAlle anzeigen":
                    for (Player current : Bukkit.getOnlinePlayers()) {
                        p.showPlayer(current);
                    }
                    p.sendMessage(Main.Prefix + "Du siehst nun alle Spieler");
                    p.closeInventory();
                    break;
                case "§6Premium anzeigen":
                    for (Player current : Bukkit.getOnlinePlayers()) {
                        if (!current.hasPermission("lobby.premium"))
                            p.hidePlayer(current);
                    }
                    p.sendMessage(Main.Prefix + "Du siehst nun Premium Spieler");
                    p.closeInventory();
                    break;
                case "§cNiemand zeigen":
                    for (Player current : Bukkit.getOnlinePlayers()) {
                        p.hidePlayer(current);
                    }
                    p.sendMessage(Main.Prefix + "Du siehst nun niemanden");
                    p.closeInventory();
                    break;
            }
        } catch (Exception ignored) {
        }
    }

}
