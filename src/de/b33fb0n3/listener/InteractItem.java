package de.b33fb0n3.listener;

import de.b33fb0n3.lumexlobby.Main;
import de.b33fb0n3.utils.ItemBuilder;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.Material;
import org.bukkit.Sound;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.block.Action;
import org.bukkit.event.inventory.InventoryType;
import org.bukkit.event.player.PlayerInteractEvent;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.ItemStack;


/**
 * Plugin made by B33fb0n3YT
 * 09.08.2020
 * F*CKING SKIDDER!
 * Licensed by B33fb0n3YT
 * © All rights reserved
 */

public class InteractItem implements Listener {

    static Inventory compass;
    static Inventory hider;

    @EventHandler
    public void onInteract(PlayerInteractEvent e) {
        Player p = e.getPlayer();

        if(e.getItem().getType().equals(Material.ENDER_PEARL))
            return;

        if (e.getAction().equals(Action.RIGHT_CLICK_AIR) || e.getAction().equals(Action.RIGHT_CLICK_BLOCK) || e.getAction().equals(Action.LEFT_CLICK_AIR) || e.getAction().equals(Action.LEFT_CLICK_BLOCK)) {
            try {
                e.setCancelled(true);
                ItemStack fuellung = new ItemBuilder(new ItemStack(Material.STAINED_GLASS_PANE, 1)).setName("§r").setDurability((short) 7).toItemStack();
                switch (e.getItem().getItemMeta().getDisplayName()) {
                    case "§cNavigator §7(Rechtsklick)":
                        int invsize = 3 * 9;
                        compass = Bukkit.createInventory(null, invsize, "§cNavigator");

                        for (int i = 0; i < invsize; i++) {
                            compass.setItem(i, fuellung);
                        }
                        compass.setItem(10, new ItemBuilder(new ItemStack((Material.GRASS), 1)).setName("§7Skyblock").toItemStack());
                        compass.setItem(12, new ItemBuilder(new ItemStack((Material.BED), 1)).setName("§7Bedwars").toItemStack());
                        compass.setItem(14, new ItemBuilder(new ItemStack((Material.IRON_SWORD), 1)).setName("§7PvP").toItemStack());
                        compass.setItem(16, new ItemBuilder(new ItemStack((Material.SANDSTONE), 1)).setName("§7BuildFFA").toItemStack());

                        p.openInventory(compass);
                        break;
                    case "§a§lPlayerhider §7(Rechtsklick)":
                        hider = Bukkit.createInventory(null, InventoryType.BREWING, "§a§lPlayerhider");

                        hider.setItem(0, new ItemBuilder(new ItemStack(Material.MELON, 1)).setName("§aAlle anzeigen").toItemStack());
                        hider.setItem(1, new ItemBuilder(new ItemStack(Material.ARROW, 1)).setName("§6Premium anzeigen").toItemStack());
                        hider.setItem(2, new ItemBuilder(new ItemStack(Material.GOLD_LEGGINGS, 1)).setName("§cNiemand zeigen").toItemStack());
                        hider.setItem(3, new ItemStack(Material.BARRIER, 1));

                        p.openInventory(hider);
                        break;
                    case "§e§l Freunde §7(Rechtsklick)":
                        p.chat("/friend list");
                        break;
                    default:
                        p.closeInventory();
                        break;
                }
            } catch (Exception e2) {
                p.closeInventory();
            }
        }
    }
}
