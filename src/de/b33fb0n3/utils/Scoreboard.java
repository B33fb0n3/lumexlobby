package de.b33fb0n3.utils;

import de.b33fb0n3.lumexlobby.Main;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.entity.Player;
import org.bukkit.scoreboard.DisplaySlot;
import org.bukkit.scoreboard.Objective;
import org.bukkit.scoreboard.Team;

/**
 * Plugin made by B33fb0n3YT
 * 09.08.2020
 * F*CKING SKIDDER!
 * Licensed by B33fb0n3YT
 * © All rights reserved
 */


public class Scoreboard {
    public static void setScoreboard(Player p) {
        org.bukkit.scoreboard.Scoreboard board = p.getScoreboard();
        Objective obj = board.getObjective("lobby") != null ? board.getObjective("lobby") : board.registerNewObjective("lobby", "dummy");

        obj.setDisplayName(ChatColor.translateAlternateColorCodes('&', Main.settings.getString("Scoreboard.Displayname")));
        obj.setDisplaySlot(DisplaySlot.SIDEBAR);

        for(int i = 1; i < 12; i++) {
            obj.getScore(ChatColor.translateAlternateColorCodes('&', Main.settings.getString("Scoreboard.line." +  i))).setScore(i);
        }
        p.setScoreboard(board);
    }
}
