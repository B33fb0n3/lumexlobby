package de.b33fb0n3.lumexlobby;

import de.b33fb0n3.commands.Author;
import de.b33fb0n3.commands.Setwarp;
import de.b33fb0n3.commands.Warp;
import de.b33fb0n3.listener.*;
import org.bukkit.Bukkit;
import org.bukkit.command.ConsoleCommandSender;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.configuration.file.YamlConfiguration;
import org.bukkit.plugin.PluginManager;
import org.bukkit.plugin.java.JavaPlugin;

import java.io.File;
import java.io.IOException;
import java.util.Arrays;
import java.util.List;

/**
 * Plugin made by B33fb0n3YT
 * 09.08.2020
 * F*CKING SKIDDER!
 * Licensed by B33fb0n3YT
 * © All rights reserved
 */

public class Main extends JavaPlugin {

    private static Main plugin;
    public ConsoleCommandSender console = getServer().getConsoleSender();
    public static String Prefix = "§eSystem §7| §f";
    public static String noPerm = Main.Prefix + "§cDafür hast du keine Rechte!";
    public static List<Integer> slots = Arrays.asList(10, 12, 14, 16);
    public static File settingsFile = new File("plugins/LobbySystem", "settings.yml");
    public static FileConfiguration settings = YamlConfiguration.loadConfiguration(settingsFile);

    @Override
    public void onEnable() {
        plugin = this;

        console.sendMessage(Prefix + "§e[]=======================[]");
        console.sendMessage(Prefix + "						 ");
        console.sendMessage(Prefix + "§2Coded by: §dB33fb0n3YT");
        loadConfig();
        console.sendMessage(Prefix + "§aLobby wurde aktiviert!");
        console.sendMessage(Prefix + "						 ");
        console.sendMessage(Prefix + "§e[]=======================[]");
        init();
    }

    private void init() {
        initCommands();
        initListener();
    }

    private void initListener() {
        PluginManager pm = Bukkit.getPluginManager();
        pm.registerEvents(new BlockPlaceBreak(), this);
        pm.registerEvents(new EntityDamage(), this);
        pm.registerEvents(new AntiFeed(), this);
        pm.registerEvents(new Antidrop(), this);
        pm.registerEvents(new Consume(), this);
        pm.registerEvents(new Explode(), this);
        pm.registerEvents(new InvClick(), this);
        pm.registerEvents(new Pickup(), this);
        pm.registerEvents(new WeatherChange(), this);
        pm.registerEvents(new Join(), this);
        pm.registerEvents(new InteractItem(), this);
    }

    private void initCommands() {
        getCommand("warp").setExecutor(new Warp());
        getCommand("setwarp").setExecutor(new Setwarp());
        getCommand("author").setExecutor(new Author());
    }

    @Override
    public void onDisable() {
        console.sendMessage(Prefix + "§e[]=======================[]");
        console.sendMessage(Prefix + "						 ");
        console.sendMessage(Prefix + "§2Coded by: §dB33fb0n3YT");
        console.sendMessage(Prefix + "§cLobby wurde deaktiviert!");
        console.sendMessage(Prefix + "						 ");
        console.sendMessage(Prefix + "§e[]=======================[]");
    }

    public static Main getPlugin() {
        return plugin;
    }

    public static void error(Exception e) {
        Main.getPlugin().console.sendMessage(Main.Prefix + "§cFEHLER: §b" + e.getLocalizedMessage());
        for(int i = 0; i < e.getStackTrace().length; i++) {
            if(e.getStackTrace()[i].toString().contains("de.b33fb0n3")) {
                Main.getPlugin().console.sendMessage("§b"+ e.getStackTrace()[i]);
            }
        }
    }

    private void loadConfig() {
        settings.options().header("Hier kannst du alles ändern.");
        settings.addDefault("Scoreboard.Displayname", "&bB33fb0n3&4.net");
        settings.addDefault("Scoreboard.line.1", "Zeile1");
        settings.addDefault("Scoreboard.line.2", "Zeile2");
        settings.addDefault("Scoreboard.line.3", "Zeile3");
        settings.addDefault("Scoreboard.line.4", "Zeile4");
        settings.addDefault("Scoreboard.line.5", "Zeile5");
        settings.addDefault("Scoreboard.line.6", "Zeile6");
        settings.addDefault("Scoreboard.line.7", "Zeile7");
        settings.addDefault("Scoreboard.line.8", "Zeile8");
        settings.addDefault("Scoreboard.line.9", "Zeile9");
        settings.addDefault("Scoreboard.line.10", "Zeile10");
        settings.addDefault("Scoreboard.line.11", "Zeile11");
        settings.options().copyDefaults(true);
        try {
            settings.save(settingsFile);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
